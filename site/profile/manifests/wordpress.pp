class profile::wordpress {

    include apache
    include mysql::server
    include wordpress
    include apache::mod::php
    
    package { 'php-mysqlnd.x86_64':
        ensure => installed,
        notify => Service['httpd'],
    }
    
    
    

}
